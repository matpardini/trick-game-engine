import { Player, ScoringDirection } from "../../../constants"
import { PlayingCard } from "../useDealer/useDealer.types";

export interface Rules {
  game: string
  deck: PlayingCard[]
  scoring: ScoringRules
  allowedPlayers: number[]
  players: Player[]
  startingHandLength: number
}

export interface GameState {
  currentGame: string
  deck: PlayingCard[]
  round: number
  setRound: (round: number) => void
  scoreCard: ScoreCard
  setScoreCard: (getNewScoreCard: (scoreCard: ScoreCard) => ScoreCard) => void
  winner: Player | undefined
}

export interface ScoringRules {
  direction: ScoringDirection
  startingScore: number
  winningScore: number
}

export interface ScoreCard {
  [player: string]: number
}