import { useGameState } from "./useGameState"
import { act, renderRecoilHook } from 'react-recoil-hooks-testing-library'
import { ScoringDirection, Player } from "../../../constants"
import { MockGame } from "../../../mocks/mockGameRules"
import { ScoreCard } from "./useGameState.types"

describe('useGameState', () => {
  describe('with descending scores', () => {
    let container: any
    beforeEach(() => {
      container = renderRecoilHook(() => useGameState(MockGame))
    })
    it('should render', () => {
      const { current } = container.result
      expect(current).toBeDefined()
    })
    it('should return current game', () => {
      const { current } = container.result
      expect(current.currentGame).toBeDefined()
    })
    // it('should return next turn and next turn setter', () => {
    //   const { current } = container.result
    //   expect(current.nextTurn).toBeDefined()
    //   expect(current.setNextTurn).toBeDefined()
    // })
    // it('should update next turn via setNextTurn', () => {
    //   const { current } = container.result
    //   expect(current.nextTurn).toEqual(Player.PLAYER_ONE)
    //   act(() => { current.setNextTurn(Player.COMPUTER) })
    //   expect(container.result.current.nextTurn).toEqual(Player.COMPUTER)
    // })
    it('should return round and setRound', () => {
      const { current } = container.result
      expect(current.round).toBeDefined()
      expect(current.setRound).toBeDefined()
    })
    it('should update round count via setRound', () => {
      const { current } = container.result
      expect(current.round).toEqual(1)
      act(() => { current.setRound((c: number) => ++c) })
      expect(container.result.current.round).toEqual(2)
    })
    it('should return the current game score', () => {
      const { current } = container.result
      expect(current.scoreCard).toEqual({
       [Player.PLAYER_ONE]: 7,
       [Player.COMPUTER]: 7
      })
      act(() => { current.setScoreCard((currentScoreCard: ScoreCard) => {
        return {
          ...currentScoreCard,
          [Player.PLAYER_ONE]: 4
        }
      })})
      expect(container.result.current.scoreCard).toEqual({
        [Player.PLAYER_ONE]: 4,
        [Player.COMPUTER]: 7
      })
    })
    it('should return the winner', () => {
      const { current } = container.result
      expect(current.hasOwnProperty('winner')).toBeTruthy()
    })
    it('should declare a winner when one player surpasses the winning score', () => {
      const { current } = container.result
      expect(current.winner).toBeUndefined()
      act(() => { current.setScoreCard((currentCard: ScoreCard) => ({ ...currentCard, [Player.COMPUTER]: 0 }))})
      expect(container.result.current.winner).toEqual(Player.COMPUTER)
    })
  })
  describe('with ascending scores', () => {
    const ascendingMockGame = {
      ...MockGame,
      scoring: {
        direction: ScoringDirection.ASCENDING,
        startingScore: 0,
        winningScore: 7
      },
    }
    let container: any
    beforeEach(() => {
      container = renderRecoilHook(() => useGameState(ascendingMockGame))
    })
    it('should declare a winner when one player surpasses the winning score', () => {
      const { current } = container.result
      expect(current.winner).toBeUndefined()
      act(() => { current.setScoreCard((currentCard: ScoreCard) => ({ ...currentCard, [Player.COMPUTER]: 7 }))})
      expect(container.result.current.winner).toEqual(Player.COMPUTER)
    })
  })
})