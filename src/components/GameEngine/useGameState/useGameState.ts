import { useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import { Player, ScoringDirection } from '../../../constants'
import { GameState, Rules, ScoreCard } from './useGameState.types'
import {
  playersState,
  scoreCardState,
  startingHandLengthState
} from '../../../atoms'

export const useGameState = ({
  game,
  deck,
  scoring,
  players: playersProp,
  startingHandLength: startingHandLengthProp
}: Rules): GameState => {
  const [ scoreCard, _setScoreCard ] = useRecoilState<ScoreCard>(scoreCardState)
  const [ players, setPlayers ] = useRecoilState<Player[]>(playersState)
  const [ startingHandLength, setStartingHandLength ] = useRecoilState<number>(startingHandLengthState)
  const [ round, setRound ] = useState<number>(1)
  const [ winner, setWinner ] = useState<Player | undefined>()
  const currentGame = game

  useEffect(() => {
    if (playersProp !== players) setPlayers(playersProp)
  }, [playersProp, players])

  useEffect(() => {
    if (startingHandLengthProp !== startingHandLength) setStartingHandLength(startingHandLengthProp)
  }, [startingHandLength])

  useEffect(() => {
    const startingScoreCard = players.reduce((card, player) => ({
      ...card, [player]: scoring.startingScore
    }), {} as ScoreCard)
    _setScoreCard(startingScoreCard)
  }, [players, _setScoreCard, scoring.startingScore])
  
  const setScoreCard = (getNewScoreCard: (currentScoreCard: ScoreCard) => ScoreCard) => {
    const currentScoreCard = getNewScoreCard(scoreCard)
    _setScoreCard(currentScoreCard)
    
    Object.entries(currentScoreCard).forEach(([player, score]) => {
      if (scoring.direction === ScoringDirection.ASCENDING) {
        if (score >= scoring.winningScore) setWinner(player as Player)
      }
      if (scoring.direction === ScoringDirection.DESCENDING) {
        if (score <= scoring.winningScore) setWinner(player as Player)
      }
    })
  }
  
  return {
    deck,
    currentGame,
    round,
    setRound,
    scoreCard,
    setScoreCard,
    winner,
  }
}