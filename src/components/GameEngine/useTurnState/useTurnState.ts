import { useRecoilState, useRecoilValue } from 'recoil'
import { TurnState } from './useTurnState.types'
import { currentTurnState, playersState } from '../../../atoms'
import { Player } from '../../../constants'

export const useTurnState = (): TurnState => {
  const [currentTurn, _setCurrentTurn] = useRecoilState<Player>(currentTurnState)
  const players = useRecoilValue(playersState)

  const getNextTurn = () => {
    const getNextTurnByIndex = (current: number) => {
      if (players.length - 1 === current) return players[0]
      return players[current + 1]
    }
    const currentIndex = players.findIndex(player => player === currentTurn)
    return getNextTurnByIndex(currentIndex)
  }
  
  const advanceTurn = (newVal?: Player) => { 
    if (!newVal) return _setCurrentTurn(getNextTurn())
    return _setCurrentTurn(newVal)
  }

  const nextTurn = getNextTurn()

  return {
    advanceTurn,
    currentTurn,
    nextTurn
  }
}