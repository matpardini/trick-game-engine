import { act, renderRecoilHook } from "react-recoil-hooks-testing-library"
import { useTurnState } from "."
import { currentTurnState, playersState } from "../../../atoms"
import { Player } from "../../../constants"

describe('useTurnState', () => {
  const players = [
    Player.PLAYER_ONE,
    Player.PLAYER_TWO,
    Player.PLAYER_THREE,
    Player.PLAYER_FOUR
  ]
  const startingTurn = Player.PLAYER_ONE
  it('should return the current turn', () => {
    const { result } = renderRecoilHook(useTurnState, {
      states: [
        { recoilState: currentTurnState, initialValue: startingTurn },
        { recoilState: playersState, initialValue: players}
      ]
    })
    expect(result.current.currentTurn).toEqual(startingTurn)
  })
  it('should return a function to advance the turn state', () => {
    const { result } = renderRecoilHook(useTurnState, {
      states: [
        { recoilState: currentTurnState, initialValue: startingTurn },
        { recoilState: playersState, initialValue: players}
      ]
    })
    expect(typeof result.current.advanceTurn).toEqual('function')
  })
  it('should return the next turn', () => {
    const { result } = renderRecoilHook(useTurnState, {
      states: [
        { recoilState: currentTurnState, initialValue: startingTurn },
        { recoilState: playersState, initialValue: players}
      ]
    })
    expect(result.current.nextTurn).toEqual(players[1])
    act(() => {
      result.current.advanceTurn()
    })
    expect(result.current.nextTurn).toEqual(players[2])
  })
  it.only('should allow for automatic or custom selection of nextTurn value', () => {
    const { result } = renderRecoilHook(useTurnState, {
      states: [
        { recoilState: currentTurnState, initialValue: startingTurn },
        { recoilState: playersState, initialValue: players}
      ]
    })
    expect(result.current.nextTurn).toEqual(players[1])
    act(() => {
      result.current.advanceTurn(players[3])
    })
    expect(result.current.currentTurn).toEqual(players[3])
    expect(result.current.nextTurn).toEqual(players[0])
  })
})