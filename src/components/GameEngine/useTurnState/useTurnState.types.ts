import { Player } from "../../../constants"

export interface TurnState {
  advanceTurn: (
    nextTurn?: Player
  ) => void
  currentTurn: Player,
  nextTurn: Player
}