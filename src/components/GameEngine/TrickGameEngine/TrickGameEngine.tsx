import React, { ReactChild } from 'react'
import { RecoilRoot, MutableSnapshot } from 'recoil'
// import { MutableSnapshot } from 'recoil'
// import { currentTurnState } from '../../atoms'

const defaultState = ({}: MutableSnapshot): void => {} 

export const TrickGameEngine: React.FC<{
  children: ReactChild
  initializeState?: (mutableSnapshot: MutableSnapshot) => void
}> = ({
  children,
  initializeState = defaultState
}) => {
  return (
    <RecoilRoot initializeState={initializeState}>
      {children}
    </RecoilRoot>
  )
}