import { renderRecoilHook } from 'react-recoil-hooks-testing-library'
import { useDealer } from '.'
import { playersState, startingHandLengthState } from '../../..'
import { Player } from '../../../constants'
import { MockGame } from '../../../mocks/mockGameRules'

describe('useDealer', () => {
  const deck = MockGame.deck
  it('should render with correct exports', () => {
    const { result } = renderRecoilHook(() => useDealer({ deck }))
    expect(Object.keys(result.current)).toContain('shuffledDeck')
    expect(Object.keys(result.current)).toContain('dealCards')
  })
  it('should shuffle cards', () => {
    const { result } = renderRecoilHook(() => useDealer({ deck }))
    deck.forEach(card => {
      expect(result.current.shuffledDeck.includes(card)).toBeTruthy()
    })
    result.current.shuffledDeck.forEach(card => {
      expect(deck.includes(card)).toBeTruthy()
    })
  })
  it('should deal cards to players', () => {
    const players = [Player.COMPUTER, Player.PLAYER_ONE]
    const { result } = renderRecoilHook(() => useDealer({ deck }), {
      states: [
        { recoilState: playersState, initialValue: players },
        { recoilState: startingHandLengthState, initialValue: MockGame.startingHandLength }
      ]
    })
    const { dealCards, shuffledDeck } = result.current
    const { hands } = dealCards(shuffledDeck)
    expect(Object.keys(hands)).toEqual(players)
  })
  it('should deal the correct amount of cards', () => {
    const players = [Player.COMPUTER, Player.PLAYER_ONE]
    const { result } = renderRecoilHook(() => useDealer({ deck }), {
      states: [
        { recoilState: playersState, initialValue: players },
        { recoilState: startingHandLengthState, initialValue: 5 }
      ]
    })
    const { dealCards, shuffledDeck } = result.current

    const { hands: hands1 } = dealCards(shuffledDeck)
    const [handOne_1, handTwo_1] = Object.values(hands1)
    expect(handOne_1.length).toEqual(5)
    expect(handTwo_1.length).toEqual(5)
  })
  it('should create a draw pile', () => {
    const players = [Player.COMPUTER, Player.PLAYER_ONE]
    const { result } = renderRecoilHook(() => useDealer({ deck }), {
      states: [
        { recoilState: playersState, initialValue: players },
        { recoilState: startingHandLengthState, initialValue: MockGame.startingHandLength }
      ]
    })
    const { dealCards, shuffledDeck } = result.current
    const { drawPile } = dealCards(shuffledDeck)
    expect(drawPile.length).toEqual(10)
  })
  it('should not include any cards already in hands in the draw pile', () => {
    const players = [Player.COMPUTER, Player.PLAYER_ONE]
    const { result } = renderRecoilHook(() => useDealer({ deck }), {
      states: [
        { recoilState: playersState, initialValue: players },
        { recoilState: startingHandLengthState, initialValue: MockGame.startingHandLength }
      ]
    })
    const { dealCards, shuffledDeck } = result.current
    const { hands, drawPile } = dealCards(shuffledDeck)
    const hasDuplicates = drawPile.reduce((hasDupe, drawCard) => {
      if (!hasDupe) {
        Object.values(hands).forEach(hand => {
          hand.forEach(card => {
            if (card.label === drawCard.label) {
              if (card.suit.label === drawCard.suit.label) {
                hasDupe = true
              }
            }
          })
        })
      }
      return hasDupe
    }, false)
    expect(hasDuplicates).toBeFalsy()
  })
  it('should pick a trump card', () => {
    const players = [Player.COMPUTER, Player.PLAYER_ONE]
    const { result } = renderRecoilHook(() => useDealer({ deck }), {
      states: [
        { recoilState: playersState, initialValue: players },
        { recoilState: startingHandLengthState, initialValue: MockGame.startingHandLength }
      ]
    })
    const { dealCards, shuffledDeck } = result.current
    const { drawPile, trump } = dealCards(shuffledDeck)
    const lastCard = drawPile[drawPile.length - 1]
    expect(trump.label).toEqual(lastCard.label)
    expect(trump.suit.label).toEqual(lastCard.suit.label)
  })
})