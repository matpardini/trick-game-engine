import { Suit, CardColor, FaceValue } from "../../../constants"

export interface SuitDetail {
  label: Suit
  color: CardColor
}

export interface Suits {
  hearts: SuitDetail
  diamonds: SuitDetail
  clubs: SuitDetail
  spades: SuitDetail
}

export interface CardDetail {
  label: FaceValue
  points: number
}

export interface DeckValues {
  two?: CardDetail
  three?: CardDetail
  four?: CardDetail
  five?: CardDetail
  six?: CardDetail
  seven?: CardDetail
  eight?: CardDetail
  nine?: CardDetail
  jack?: CardDetail
  queen?: CardDetail
  king?: CardDetail
  ten?: CardDetail
  ace?: CardDetail
}

export type PlayingCard = CardDetail & {
  id: number
  suit: SuitDetail
}

export type PlayerHand = Array<PlayingCard>

export interface PlayerHands {
  [Player: string]: PlayerHand
}

export interface UseDealerProps {
  deck: PlayingCard[]
}

export interface GameDetails {
  shuffledDeck: PlayingCard[]
  dealCards: (
    shuffledDeck: PlayingCard[],
  ) => {
    hands: PlayerHands
    drawPile: PlayingCard[]
    trump: PlayingCard
  },
}