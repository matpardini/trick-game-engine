import { GameDetails, PlayerHands, PlayingCard, UseDealerProps } from './useDealer.types'
import { useRecoilValue } from 'recoil'
import { playersState, startingHandLengthState } from '../../..'

export const useDealer = ({
  deck = []
}: UseDealerProps): GameDetails => {
  const players = useRecoilValue(playersState)
  const startingHandLength = useRecoilValue(startingHandLengthState)

  const shuffledDeck = deck.sort((a,b) =>
    Math.random() * 1 > .5
      ? a.id - b.id
      : b.id - a.id
  )

  const dealCards = (shuffledDeck: PlayingCard[],) => {
    const defaultPlayerHands = players.map(player => ({ name: player, hand: [] }))
 
    const theDeal = shuffledDeck.reduce((piles, card, i) => {
      const dealToPlayer = (player: number) => {
        const playerHand = piles[player - 1].hand
        const drawPile = piles[piles.length - 1].hand
        if (playerHand.length < startingHandLength) playerHand.push(card)
        else drawPile.push(card)
      }
    
      if (!(i % 4) && players.length >=4 ) {
        dealToPlayer(4)
        return piles
      }
      if (!(i % 3) && players.length >=3 ) {
        dealToPlayer(3)
        return piles
      }
      if (!(i % 2)) {
        dealToPlayer(2)
        return piles
      }
      dealToPlayer(1)
      return piles
    }, [
      ...defaultPlayerHands,
      { name: 'drawPile', hand: [] }
    ] as Array<{name: string, hand: PlayingCard[]}>)

    const {
      hands,
      drawPile
    } = theDeal.reduce((piles, pile) => {
      if (pile.name === 'drawPile') {
        piles.drawPile = pile.hand
        return piles
      }
      piles.hands[pile.name] = pile.hand
      return piles
    },{
      hands: {},
      drawPile: []
    } as {
      hands: PlayerHands,
      drawPile: PlayingCard[]
    })

    return {
      hands,
      drawPile,
      trump: drawPile[drawPile.length - 1]
    }
  }

  return {
    shuffledDeck,
    dealCards,
    // trickScores,
    // gameScores,
    // handState,
    // gameState,
  }
}