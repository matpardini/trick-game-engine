export enum Player {
  PLAYER_ONE = 'PlayerOne',
  PLAYER_TWO = 'PlayerTwo',
  PLAYER_THREE = 'PlayerThree',
  PLAYER_FOUR = 'PlayerFour',
  COMPUTER = 'Computer'
}