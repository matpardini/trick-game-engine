export enum ScoringDirection {
  ASCENDING = 'asc',
  DESCENDING = 'dsc'
}