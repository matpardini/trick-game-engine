import { atom } from "recoil"

export const startingHandLengthState = atom<number>({
  key: 'startingHandLengthState', // unique ID (with respect to other atoms/selectors)
  default: 10, // default value (aka initial value)
})