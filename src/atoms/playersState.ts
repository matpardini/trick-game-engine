import { atom } from "recoil"
import { Player } from "../constants"

export const playersState = atom<Player[]>({
  key: 'playersState', // unique ID (with respect to other atoms/selectors)
  default: [], // default value (aka initial value)
})
