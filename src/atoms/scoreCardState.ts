import { atom } from "recoil"
import { ScoreCard } from "../components/GameEngine/useGameState/useGameState.types"

export const scoreCardState = atom<ScoreCard>({
  key: 'scoreCardState', // unique ID (with respect to other atoms/selectors)
  default: {} as ScoreCard, // default value (aka initial value)
})