import { atom } from "recoil"
import { Player } from "../constants"

export const currentTurnState = atom<Player>({
  key: 'currentTurnState', // unique ID (with respect to other atoms/selectors)
  default: Player.PLAYER_ONE, // default value (aka initial value)
})
