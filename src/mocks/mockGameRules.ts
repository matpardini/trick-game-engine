import { PlayingCard, DeckValues, Suits } from "../components/GameEngine/useDealer/useDealer.types"
import { Suit } from "../constants/Suit"
import { CardColor } from "../constants/CardColor"
import { FaceValue } from "../constants/FaceValue"
import { Player } from "../constants"
import { ScoringDirection } from "../constants/ScoringDirection"

const { HEARTS, DIAMONDS, CLUBS, SPADES } = Suit
const { RED, BLACK } = CardColor
const { JACK, QUEEN, KING, TEN, ACE } = FaceValue

const suits: Suits = {
  [HEARTS]: { label: HEARTS, color: RED },
  [DIAMONDS]: { label: DIAMONDS, color: RED },
  [CLUBS]: { label: CLUBS, color: BLACK },
  [SPADES]: { label: SPADES, color: BLACK }
}

const mockDeckValues: DeckValues = {
  [JACK]: { label: JACK, points: 2, },
  [QUEEN]: { label: QUEEN, points: 3 },
  [KING]: { label: KING, points: 4 },
  [TEN]: { label: TEN, points: 10, },
  [ACE]: { label: ACE, points: 11 }
}

const deck: PlayingCard[] = Object.values(mockDeckValues).reduce((deck, details) => {
  const currentCardInAllSuits = Object.values(suits).map((suit, i) => ({ ...details, suit, id:(i+1) + deck.length }))
  return [ ...deck, ...currentCardInAllSuits ]
}, [])

export const MockGame = {
  game: 'Mock',
  deck,
  scoring: {
    direction: ScoringDirection.DESCENDING,
    startingScore: 7,
    winningScore: 0
  },
  allowedPlayers:[2,4],
  players:[
    Player.PLAYER_ONE,
    Player.COMPUTER
  ],
  startingHandLength: 5
}